var express = require('express');
var session = require('express-session');
var connection  = require('express-myconnection');
var mysql = require('mysql'); 
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var user = require('./routes/user');

var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();

var app = express();
app.set('view engine', 'ejs');
app.use(expressValidator());
app.use(session(
    {
        secret: 'Mop questionare',
        resave: true,
        saveUninitialized: true,
        cookie: { maxAge: 600000 }
    }
));

app.use('/css',express.static(__dirname + '/css'));
app.use('/js',express.static(__dirname + '/js'));

app.use(    
    connection(mysql,{
            
        host: 'localhost', //'localhost',
        user: 'root',
        password : 'root',
        port : 3306, //port mysql
        database:'mop',
        multipleStatements: false,
    
    },'pool') //or single
    
);

app.get('/',user.login);

app.get('/login',user.login);
app.post('/login',urlencodedParser,user.login);

app.get('/signup',user.signup);
app.post('/signup',urlencodedParser ,user.signup);

app.get('/home/dashboard', user.dashboard);
app.get('/home/questions/view', user.getQuestions);

app.get('/home/questions/add', user.adminAddQuestions);
app.post('/home/questions/add',jsonParser, user.adminSaveQuestion);

app.get('/home/questions/answer', user.userAnswerQuestions);
app.post('/home/questions/answer',urlencodedParser, user.userAnswerQuestionsPOST);

app.delete('/home/questions/delete',urlencodedParser, user.deleteQuestion);
app.get('/home/logout', user.logout);

var server = app.listen(3000, function () {

    var host = server.address().address
    var port = server.address().port
  
    console.log("Example app listening at http://%s:%s", host, port)
  
});