var bcrypt = require('bcrypt');
const saltRounds = 10;


//-----------------------------------------------login page call------------------------------------------------------
exports.login = function(req, res){

    if(req.method == "GET"){
        res.render("login",{errorLogin:""});
    }
    else if(req.method == "POST"){
        req.getConnection(function(err,connection){
            var query = connection.query("SELECT * FROM User WHERE username = ?" ,req.body.username,function(err,result,fields){
                if(err)throw err;
                if(result.length){
                    bcrypt.compare(req.body.password,result[0].password,function(err,same){
                        if(err)throw err;
                        else if(same){
                            req.session.userId = result[0].id;
                            req.session.user = result[0];
                            res.redirect('home/dashboard');
                        }
                        else{
                            res.render("login",{ errorLogin: "Wrong credentials!" });
                        }
                    });
                }
                else{
                    res.render("login",{ errorLogin: "Wrong credentials!" });
                } 
            });
        });
    }    
};

//---------------------------------------------signup page call------------------------------------------------------
exports.signup = function(req,res){
    
    if(req.method == "GET"){       
        res.render("signup",{validationErrors:[] , signUpSuccess: {} });
    }
    
    else if(req.method == "POST"){

        req.getConnection(function(err,connection){

            req.checkBody('firstName', 'First name cannot be empty.').notEmpty();
            req.checkBody('lastName', 'Last name cannot be empty.').notEmpty();
            req.checkBody('email', 'Invalid email.').isEmail();
            req.checkBody('password', 'Password must be between 5-50 characters long').len(5,50);
            req.checkBody('repeatPassword', 'Passwords do not match').equals(req.body.password);

            const validationErrors = req.validationErrors();
            if(validationErrors){
                console.log("Validation errors")
                console.log(JSON.stringify(validationErrors));
                res.render("signup",{ validationErrors: validationErrors , signUpSuccess: {} })
            }
            else{
                var user = {
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    username: req.body.firstName+'.'+req.body.lastName,
                    password: req.body.password,
                    email: req.body.email
                }
                checkEmailBeforeInsert(user , req , res,connection);
            }
        });
    }
}

function checkEmailBeforeInsert(user , req , res,connection){

    connection.query("SELECT * FROM User WHERE email = ?" ,user.email,function(err,result,fields){
        if(err)throw err;

        if(result.length == 0){
            checkUsernameBeforeInsert(user , req , res,connection);
        }
        else if(result.length > 0){
            res.render("signup",{ validationErrors: [{msg:"Email " + user.email +" already exists"}] , signUpSuccess: {} });
        }            
    });    
}

function checkUsernameBeforeInsert(user , req , res,connection){

    connection.query("SELECT * FROM User WHERE username = ?", user.username,function(err,result,fields){
        if(err)throw err;
        if(result.length == 0){
            insertUser(user , req , res,connection);
        }
        else if(result.length > 0){
            res.render("signup",{ validationErrors: [{msg:"Username " + user.username +" already exists"}] , signUpSuccess: {} });
        }
    });
}

function insertUser(user , req , res, connection){
    bcrypt.hash(user.password, saltRounds, function(err, hash) {
        if(err)throw err;
        var sql = "INSERT INTO User(username,password,firstName,lastName,email) VALUES(?,?,?,?,?)";
        var values = [user.username.toLowerCase(), hash, user.firstName, user.lastName, user.email];
        connection.query(sql , values , function(err,result,fields){
            if(err)throw err;
            res.render("signup",{ validationErrors: [] , signUpSuccess: {username: user.username.toLowerCase()} });
        });
    });
}

//---------------------------------------------questions page call------------------------------------------------------
exports.getQuestions = function(req , res){
    var user = req.session.user;
    if(user == null){
        res.redirect('/login');
    }
    else if(user.type==="Admin"){
        req.getConnection(function(err,connection){
            if(err)throw err;
            
            var sql = "SELECT * FROM Question question WHERE question.active = TRUE";
            connection.query(sql,function(error,result,fields){
                if(err)throw err;
                res.render('questions-admin' , { user: user , questions: result });
            });
            
        });
    }
    else if(user.type==="RegularUser"){
        req.getConnection(function(err,connection){
            if(err)throw err;

            var sql = "SELECT question.id , question.question , question.questionType FROM Question question "+
            " WHERE question.active = TRUE AND question.id NOT IN "+
            " ( SELECT  userQuestionAnswers.question FROM UserQuestionAnwers userQuestionAnswers WHERE userQuestionAnswers.user = ? )";
            
            /* var sql = "SELECT question.id , question.question , question.questionType FROM Question question "+
            " LEFT JOIN UserQuestionAnwers userQuestionAnswers ON userQuestionAnswers.question = question.id  "+
            " WHERE question.active = TRUE AND userQuestionAnswers.id IS NULL AND userQuestionAnswers.user !="+user.id; */
            
            connection.query(sql,[user.id],function(error,result,fields){
                if(err)throw err;
                res.render('questions-user' , { user: user , questions: result });                    
            });            
        });
    }
}

//---------------------------------------------add/edit questions page call------------------------------------------------------
exports.adminAddQuestions = function(req , res){
    var user = req.session.user;    
    if(user == null){
        res.redirect('/login');
    }
    else if(user.type === 'Admin'){        
        if(req.query.id){
            req.getConnection(function(err,connection){
                if(err)throw err;
                
                var questionSql = "SELECT question.id, question.question, question.questionType " +
                " FROM Question question WHERE question.active = TRUE AND question.id = "+req.query.id;
                connection.query(questionSql,function(error,result1,fields){                    
                    if(error)throw error;                        
                    
                    if(result1.length>0){                        
                        var questionAnswersSql = "SELECT questionAnswers.questionAnswer" +
                        " FROM QuestionAnswers questionAnswers WHERE questionAnswers.question = "+req.query.id;                            
                        connection.query(questionAnswersSql,function(error,result2,fields){
                            if(error)throw error;
                            var questionAnswers = [];
                            for(var i = 0; i<result2.length ; i++){
                                questionAnswers.push({questionAnswer : result2[i].questionAnswer});
                            }
                            var questionObject = {
                                questionId: req.query.id,
                                question : result1[0].question,
                                questionType: result1[0].questionType,
                                questionAnswers : questionAnswers
                            }
                                console.log("GET_QUESTION_OK");
                                console.log(questionObject);
                            res.render('add-questions' , { user: user ,status : "GET_QUESTION_OK" , requestedQuestion: questionObject });
                        });
                    }
                    else{
                        console.log("No question")
                        res.render('add-questions' , { 
                            user: user , 
                            status : "GET_QUESTION_ERROR",
                            message : "No question with id " + req.query.id ,
                        });
                    }
                });
            });
        }
        else{
            res.render('add-questions' , { user: user , status : "OK" , requestedQuestion: null});
        }
    }
    else{
        res.redirect('/login');
    }
}

//---------------------------------------------answer question page call------------------------------------------------------

exports.userAnswerQuestions = function(req, res){
    var user = req.session.user;
    if(user == null){
        res.redirect('/login');
    }
    else if(user.type === 'RegularUser'){
        answerQuestion(req,res,user);
    }
    else{
        res.redirect('/login');
    }
}

function answerQuestion(req,res,user){
    if(req.query.id){
        req.getConnection(function(err,connection){
            if(err)throw err;
            else{
                var questionSql = "SELECT question.id, question.question, question.questionType " +
                " FROM Question question WHERE question.active = TRUE AND question.id = "+req.query.id;
                connection.query(questionSql,function(error,result1,fields){
                    if(error)throw error;                        
                    if(result1.length>0){
                        var questionAnswersSql = "SELECT questionAnswers.id, questionAnswers.questionAnswer" +
                        " FROM QuestionAnswers questionAnswers WHERE questionAnswers.question = "+req.query.id;
                        
                        connection.query(questionAnswersSql,function(error,result2,fields){

                            if(error)throw error;
                            
                            var questionAnswers = [];
                            console.log(result2);
                            for(var i = 0; i<result2.length ; i++){
                                questionAnswers.push({
                                    questionAnswerId:result2[i].id,
                                    questionAnswer : result2[i].questionAnswer
                                });
                            }
                            
                            var questionObject = {
                                questionId: req.query.id,
                                question : result1[0].question,
                                questionType: result1[0].questionType,
                                questionAnswers : questionAnswers
                            }
                            console.log("GET_QUESTION_OK");
                            console.log(questionObject);
                            console.log(user);
                            res.render('answer-questions' , { user: user ,status : "GET_QUESTION_OK" , requestedQuestion: questionObject });
                        });
                    }
                    else{
                        console.log("No question")
                        res.render('answer-questions' , { 
                            user: user , 
                            status : "GET_QUESTION_ERROR",
                            message : "No question with id " + req.query.id ,
                        });
                    }
                });
            }
        });
    }

}

exports.userAnswerQuestionsPOST = function(req, res){
    var user = req.session.user;
    var questionType = req.query.type;

    if(user == null){
        res.redirect('/login');
    }
    else if(user.type === 'RegularUser'){

        if(questionType ==="Single choice"){
            insertSingleChoiceAnswer(req,res);
        }
        else if(questionType ==="Multiple choices"){
            insertMultipleChoicesAnswer(req,res);
        }
        else if(questionType ==="Text"){
            insertTextAnswer(req,res);
        }
        else{
            res.redirect("/home/questions/answer");
        }
    }
    else{
        res.redirect('/login');
    }
}

function insertSingleChoiceAnswer(req,res){
    var user = req.session.user;
    var questionId = req.query.id;
    var questionAnswers = [];
    for(var key in req.body ){        

        if(key.startsWith("answerRadioId")){
            var questionAnswerId = key.replace("answerRadioId","");
            questionAnswers.push([ user.id,questionId ,questionAnswerId]);
        }
    }
    req.getConnection(function(error,connection){
        var insertSql ="INSERT INTO UserQuestionAnwers(user,question,questionAnswer) VALUES ?"
        connection.query(insertSql,[questionAnswers],function(error,result,field){
            if(error)throw error;
            console.log("Number of records inserted: " + result.affectedRows);
            res.render('answer-questions' , { user: user ,status : "ANSWER_SUCCESS",message : "Successfully answered question"});
        });
    });    
}

function insertMultipleChoicesAnswer(req,res){
    var user = req.session.user;
    var questionId = req.query.id;
    var questionAnswers = [];
    for(var key in req.body ){        
        if(key.startsWith("answerCheckBoxId")){
            var questionAnswerId = key.replace("answerCheckBoxId","");
            questionAnswers.push([ user.id,questionId ,questionAnswerId]);
        }
    }

    req.getConnection(function(error,connection){
        var insertSql ="INSERT INTO UserQuestionAnwers(user,question,questionAnswer) VALUES ?"
        connection.query(insertSql,[questionAnswers],function(error,result,field){
            if(error)throw error;
            console.log("Number of records inserted: " + result.affectedRows);
            res.render('answer-questions' , { user: user ,status : "ANSWER_SUCCESS",message : "Successfully answered question"});
        });
    });
}

function insertTextAnswer(req,res){

    var user = req.session.user;
    var questionId = req.query.id;
    var questionAnswer = req.body.answerText;
    var questionAnswers = [ [user.id,questionId ,questionAnswer]];
    req.getConnection(function(error,connection){
        var insertSql ="INSERT INTO UserQuestionAnwers(user,question,answerText) VALUES ?"
        connection.query(insertSql,[questionAnswers],function(error,result,field){
            if(error)throw error;
            console.log("Number of records inserted: " + result.affectedRows);
            res.render('answer-questions' , { user: user ,status : "ANSWER_SUCCESS",message : "Successfully answered question"});
        });
    });
}

//---------------------------------------------save question page call------------------------------------------------------
exports.adminSaveQuestion = function(req , res){
    var user = req.session.user;
    if(user == null){
        res.redirect('/login');
    }
    else if(user.type === 'Admin'){
        if(req.method == "POST"){
            validateDataBeforeAddingQuestion(req,res);
        }
    }
    else{
        res.redirect('/login');
    }
}

function validateDataBeforeAddingQuestion(req,res){
    
    var oldQuestion = req.body.oldQuestion
    var newQuestion = req.body.newQuestion;
    var newQuestionType = req.body.newQuestionType;
    var newQuestionAnswers = req.body.newQuestionAnswers;
    req.getConnection(function(err,connection){
        if(err)throw err;        
        deactivateOldQuestion(req,res,connection);
    });
}

function deactivateOldQuestion(req,res, connection){

    var oldQuestion = req.body.oldQuestion
    if(oldQuestion!=null){
        connection.query("UPDATE Question SET active = false WHERE id = ?",[oldQuestion],function(error,result,fields){
            if(error)throw error
            console.log("Updated old question");
            insertNewQuestionWithoutTransaction(req,res,connection);
        });
    }
    else{
        insertNewQuestionWithoutTransaction(req,res,connection);
    }         
}

function insertNewQuestionWithoutTransaction(req,res,connection){
    var newQuestion = req.body.newQuestion;
    var newQuestionType = req.body.newQuestionType;
    var newQuestionAnswers = req.body.newQuestionAnswers;
    connection.query("INSERT INTO Question(question, questionType) VALUES(?,?) ",[newQuestion , newQuestionType],function(error,result,fields){
        if (error) throw error;
        
        questionId = result.insertId
        var insertValues = [];
        for(var i=0; i<newQuestionAnswers.length; i++){
            if(newQuestionType === "Text"){
                insertValues.push([questionId]);
            }
            else{
                insertValues.push([questionId , newQuestionAnswers[i].answer]);
            }
        }
        if(newQuestionType !== "Text"){
            insertReqularQuestionAnswer(req,res,connection , insertValues);
        }
        else{
            res.json({
                user: req.session.user , 
                status : "QUESTION_CREATED",
                message : "Question saved",
                requestedQuestion : {}
            });
        }
        
    });   
}

function insertReqularQuestionAnswer(req,res,connection , insertValues){
    
    connection.query("INSERT INTO QuestionAnswers(question, questionAnswer) VALUES ?",[insertValues],function(error,result2,fields){
        if (error) throw error;
        else{
            console.log("Number of records inserted: " + result2.affectedRows);
            res.json({
                user: req.session.user , 
                status : "QUESTION_CREATED",
                message : "Question saved",
                requestedQuestion : {}
            });
        }
    });
}

//---------------------------------------------delete question page call------------------------------------------------------
exports.deleteQuestion = function(req , res){
    var user = req.session.user;
    if(user == null){
        res.redirect('/login');
    }
    else if(user.type === 'Admin'){        
        if(req.query.id){
            req.getConnection(function(err,connection){
                if(err)throw err;
                connection.query("DELETE FROM Question WHERE id = ?",[req.query.id],function(error,result,fields){
                    if(err)throw err;
                    if(result.affectedRows>0){
                        res.json({ response :"success"});
                    }
                    else{
                        res.json({response : "fail"});
                    }
                });
            });
        }
        else{
            res.redirect('/login');
        }   
    }
    else{
        res.redirect('/login');
    }
}


//---------------------------------------------welcome page call------------------------------------------------------
exports.dashboard = function(req , res){
    var user = req.session.user;
    if(user == null){
        res.redirect('/login');
    }
    else if(user.type === 'Admin'){
        res.render('welcome-admin' , { user:user });
    }
    else{
        res.render('welcome-user' , { user:user } );
    }    
}

exports.logout = function(req,res){
    req.session.destroy(function(error){
        res.redirect("/login");
    });
}