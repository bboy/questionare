-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 14, 2018 at 11:32 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mop`
--

-- --------------------------------------------------------

--
-- Table structure for table `Question`
--

CREATE TABLE `Question` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(256) CHARACTER SET utf8 NOT NULL,
  `questionType` enum('Single choice','Multiple choices','Text','') NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `approved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `QuestionAnswers`
--

CREATE TABLE `QuestionAnswers` (
  `id` int(11) NOT NULL,
  `question` int(11) NOT NULL,
  `questionAnswer` varchar(256) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `QuestionType`
--

CREATE TABLE `QuestionType` (
  `id` int(11) UNSIGNED NOT NULL,
  `Value` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `QuestionType`
--

INSERT INTO `QuestionType` (`id`, `Value`) VALUES
(1, 'Single choice'),
(2, 'Multiple choice'),
(3, 'Text');

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `id` int(11) NOT NULL,
  `username` varchar(20) CHARACTER SET utf8 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL,
  `firstName` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lastName` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `type` enum('Admin','RegularUser') CHARACTER SET utf8 NOT NULL DEFAULT 'RegularUser'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`id`, `username`, `password`, `firstName`, `lastName`, `email`, `type`) VALUES
(1, 'admin', '$2a$10$ibVuyadFQdMCP1JIUOyLtevW2Xb9iw.Pmirh5ndtA/.IWs.T228fi', 'admin', 'admin', 'admin@admin.com', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `UserQuestionAnwers`
--

CREATE TABLE `UserQuestionAnwers` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `question` int(10) UNSIGNED NOT NULL,
  `questionAnswer` int(11) DEFAULT NULL,
  `answerText` varchar(500) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Question`
--
ALTER TABLE `Question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `QuestionAnswers`
--
ALTER TABLE `QuestionAnswers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `QuestionType`
--
ALTER TABLE `QuestionType`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `username_2` (`username`,`password`);

--
-- Indexes for table `UserQuestionAnwers`
--
ALTER TABLE `UserQuestionAnwers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `questionAnswers` (`questionAnswer`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Question`
--
ALTER TABLE `Question`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
--
-- AUTO_INCREMENT for table `QuestionAnswers`
--
ALTER TABLE `QuestionAnswers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT for table `QuestionType`
--
ALTER TABLE `QuestionType`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `UserQuestionAnwers`
--
ALTER TABLE `UserQuestionAnwers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
